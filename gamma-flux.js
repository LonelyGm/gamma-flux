import { GFCONFIG } from "./module/config.js";
import GearSheetGw from "./module/gear/sheet.js";
import CharacterSheetGw from "./module/character/sheet.js";

Hooks.once('init', function () {
  console.log(`gamma-flux | Initializing the gamma-flux Game System\n${GFCONFIG.ASCII}`);

  CONFIG.GFCONFIG = GFCONFIG;

  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("gamma-flux", CharacterSheetGw, {
    makeDefault: true,
    label: "gamma-flux.CharacterSheetGw"
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("gamma-flux", GearSheetGw, {
    makeDefault: true,
    label: "gamma-flux.GearSheetGw"
  });

});