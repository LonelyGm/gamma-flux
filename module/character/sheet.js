/**
 * An Actor sheet for player character type actors.
 * @type {CharacterSheetGw}
 */
export default class CharacterSheetGw extends ActorSheet {
	constructor(...args) {
    super(...args);
  }

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			 template: "systems/gamma-flux/templates/actor/character/sheet.html",
			 classes: ["gamma-flux", "sheet", "character"],
			 width: 720,
			 height: 680
		});
	}

	/* -------------------------------------------- */
  /** @inheritdoc */
  get template() {
    const path = "systems/gamma-flux/templates/item/gear";
    return `${path}/${this.item.data.type}.html`;
  }

	/* -------------------------------------------- */
	getData() {
    const sheetData = super.getData();

    // Return data for rendering
    return sheetData;
  }
}