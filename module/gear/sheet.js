export default class GearSheetGw extends ItemSheet {
  constructor(...args) {
    super(...args);
    // Expand the default size of the class sheet
    if ( this.object.data.type === "class" ) {
      this.options.width = this.position.width =  600;
      this.options.height = this.position.height = 680;
    }
  }

  /* -------------------------------------------- */
  /** @inheritdoc */
  get template() {
    const path = "systems/gamma-flux/templates/item/gear";
    return `${path}/${this.item.data.type}.html`;
  }

  /* -------------------------------------------- */
  /** @override */
  async getData(options) {
    const data = super.getData(options);
    const itemData = data.data;

    data.config = CONFIG.GFCONFIG;

    // Re-define the template data references (backwards compatible)
    data.item = itemData;
    data.data = itemData.data;

    return data;
  }
}