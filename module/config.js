export const GFCONFIG = {};

// ASCII Artwork
GFCONFIG.ASCII = `
 ██████╗  █████╗ ███╗   ███╗███╗   ███╗ █████╗     ███████╗██╗     ██╗   ██╗██╗  ██╗
██╔════╝ ██╔══██╗████╗ ████║████╗ ████║██╔══██╗    ██╔════╝██║     ██║   ██║╚██╗██╔╝
██║  ███╗███████║██╔████╔██║██╔████╔██║███████║    █████╗  ██║     ██║   ██║ ╚███╔╝ 
██║   ██║██╔══██║██║╚██╔╝██║██║╚██╔╝██║██╔══██║    ██╔══╝  ██║     ██║   ██║ ██╔██╗ 
╚██████╔╝██║  ██║██║ ╚═╝ ██║██║ ╚═╝ ██║██║  ██║    ██║     ███████╗╚██████╔╝██╔╝ ██╗
 ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝    ╚═╝     ╚══════╝ ╚═════╝ ╚═╝  ╚═╝
 By LonelyGm, a game for a wayward mind`;

GFCONFIG.techTypes = {
	tech0: "GF.techNone",
	tech1: "GF.tech1",
	tech2: "GF.tech2",
	tech3: "GF.tech3",
	tech4: "GF.tech4",
	tech5: "GF.tech5",
	techS: "GF.techS"
}

GFCONFIG.rarityTypes = {
	buyable: "GF.rarityBuyable",
	treasure: "GF.rarityTreasure",
	junk: "GF.rarityJunk"
}

GFCONFIG.armorTypes = {
	armor: "GF.armorTypeArmor",
	shield: "GF.armorTypeShield",
	field: "GF.armorTypeField"
}

GFCONFIG.weaponTypes = {
	melee: "GF.weaponMelee",
	explosive: "GF.weaponExplosive",
	ranged: "GF.weaponRanged",
	pistol: "GF.weaponPistol",
	tool: "GF.weaponTool",
	rifle: "GF.weaponRifle"
}